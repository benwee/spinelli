FROM openjdk:8-jdk-alpine

ARG JAR_FILE
COPY ${JAR_FILE} myapp.jar

# Make port 8081 available to the world outside this container. Minor change to trigger TeamCity build.
EXPOSE 8081

# Download small demo log file from remote URL and copy into Docker image.
ADD http://olsensoft.com/sg18/ad/ProjectResources/fixdata3.log /var/tmp/fixdata3.log

# Allow read access to file, for all users. 
RUN chmod a+r /var/tmp/fixdata3.log

ENTRYPOINT ["java","-jar","/myapp.jar"] 