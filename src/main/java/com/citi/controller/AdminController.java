package com.citi.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

	@Value("${logpath}")
	String logpath;
	
	@RequestMapping(method=RequestMethod.GET, 
			value="/admin", 
			headers="Accept=application/json")
	public Collection<String> loadLogData() {
		
		System.out.printf("Getting file %s\n", logpath);
		
		List<String> lines = new ArrayList<>();

		/*
		File f = new File(logpath);
		if (f.exists()) {
			lines.add("File " + logpath + " exists");
		}
		else {
			lines.add("File " + logpath + " doesn't exist");
		}
		*/
		
		try (BufferedReader br = new BufferedReader(new FileReader(logpath))) {			
			
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
				lines.add(s);
			}
		}
		catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		
		return lines;
	}
}
