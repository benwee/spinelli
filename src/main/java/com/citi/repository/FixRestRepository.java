package com.citi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import com.citi.entity.Order;
import com.citi.entity.Trade;


@Component
public interface FixRestRepository extends MongoRepository<Order, Integer> {
		
		
}
