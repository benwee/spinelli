package com.citi.repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.citi.entity.Order;
import com.citi.entity.Trade;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Component
public class FixMongoRepository{
	@Autowired
	MongoTemplate template;
	
	@Transactional
	public void insertManyDocuments(List<Trade> tList) {
		//Initializing bulk operations
		BulkOperations ops = template.bulkOps(BulkOperations.BulkMode.ORDERED, Trade.class);
		ops.insert(tList);
		ops.execute();
	}
	@Transactional
	public void insertManyOrderDocuments(List<Order> tList) {
		//Initializing bulk operations
		BulkOperations ops = template.bulkOps(BulkOperations.BulkMode.ORDERED, Order.class);
		ops.insert(tList);
		ops.execute();
	}
	
	public Trade findMinTimeOfNewIOC(){
		Query query = new Query();
		query.addCriteria(Criteria.where("timeInForce").is(3));
		query.addCriteria(Criteria.where("orderStatus").is(0));
		query.with(new Sort(Sort.Direction.ASC,"time"));
		return template.findOne(query,Trade.class);
	}
	
	public Trade findMaxTimeOfIOC(){
		Query query = new Query();
		query.addCriteria(Criteria.where("timeInForce").is(3));
		query.with(new Sort(Sort.Direction.DESC,"time"));
		return template.findOne(query,Trade.class);
	}
	
	
	public int sumNewIOCQtyByTimeBtw(Date start, Date end) {
		MatchOperation match = Aggregation
			    .match(new Criteria().andOperator(Criteria.where("timeInForce").is(3),
			    		Criteria.where("orderStatus").is(0),
			    		Criteria.where("time").gt(start).lte(end)));
		GroupOperation group = Aggregation
				.group("orderStatus").sum("orderQty").as("total");
		Aggregation aggregation = Aggregation.newAggregation(match,group);
		AggregationResults<Result> total = template.aggregate(aggregation,Trade.class,Result.class);
		return total.getUniqueMappedResult().getTotal();
	}
	
	public int sumFilledIOCQtyByTimeBtw(Date start, Date end) {
		MatchOperation match = Aggregation
			    .match(new Criteria().andOperator(Criteria.where("timeInForce").is(3),
			    		Criteria.where("orderStatus").is(2),
			    		Criteria.where("time").gt(start).lte(end)));
		GroupOperation group = Aggregation
				.group("orderStatus").sum("orderQty").as("total");
		Aggregation aggregation = Aggregation.newAggregation(match,group);
		AggregationResults<Result> total = template.aggregate(aggregation,Trade.class,Result.class);
		return total.getUniqueMappedResult().getTotal();
	}
	
	
	 class Result{
		private int total;
		
		int getTotal() {
			return total;
		}
	}
}
