package com.citi.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import com.citi.entity.Trade;

@Component
public interface FixRestTradeRepository extends MongoRepository<Trade,Integer> {
	List<Trade> findTop10ById(int Id, List<Trade> ids);
	
	List<Trade> findTradeByOrderStatus(int value);
	
	List<Trade> findTop100ByOrderStatus(int value);
	
	List<Trade> findTradeByTimeBetweenAndOrderStatus(
			Timestamp startTime,Timestamp endTime,int orderStatus);
	
	List<Trade> findTradeByOrderStatusAndTimeInForce(int orderStatus,int timeInForce);
	
}
