package com.citi.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.annotation.Generated;

import org.springframework.data.annotation.Id;


public class Order {
  
 private int _id;
 
 private Date time;
 private String symbol;
 private double brokerCom;
 private double tradeCost;
 //private double iocHitRate;
 private double high;
 private double low;
 private double open;
 private double close;
 private double volume;

 public Order() {
  // Empty no-arg constructor.
 }
 
// public Order(Date time,
//     String symbol) {
//  
// }
 
 public Order(int id,
     Date time, 
     //String clientOrderID,  
     String symbol, 
     double brokerCom, 
     double tradeCost,
     //double iocHitRate,
     double high,
     double low,
     double open,
     double close,
     double vwap
     ){
  this._id = id;
  this.time = time;
  this.symbol = symbol;
  this.brokerCom = brokerCom;
  this.tradeCost = tradeCost;
  //this.iocHitRate = iocHitRate;
  this.high = high;
  this.low = low;
  this.open = open;
  this.close = close;
  this.volume = vwap;
 }
 

 public int getId() {
  return _id;
 }

 public void setId(int id) {
  this._id = id;
 }

 public Date getTime() {
  return time;
 }

 public void setTime(Date time) {
  this.time = time;
 }

 public String getSymbol() {
  return symbol;
 }

 public void setSymbol(String symbol) {
  this.symbol = symbol;
 }

 public double getBrokerCom() {
  return brokerCom;
 }

 public void setBrokerCom(double brokerCom) {
  this.brokerCom = brokerCom;
 }

 public double getTradeCost() {
  return tradeCost;
 }

 public void setTradeCost(double tradeCost) {
  this.tradeCost = tradeCost;
 }

// public double getIocHitRate() {
//  return iocHitRate;
// }
//
// public void setIocHitRate(double iocHitRate) {
//  this.iocHitRate = iocHitRate;
// }

 public double getHigh() {
  return high;
 }

 public void setHigh(double high) {
  this.high = high;
 }

 public double getLow() {
  return low;
 }

 public void setLow(double low) {
  this.low = low;
 }

 public double getOpen() {
  return open;
 }

 public void setOpen(double open) {
  this.open = open;
 }

 public double getClose() {
  return close;
 }

 public void setClose(double close) {
  this.close = close;
 }

 public double getVolume() {
  return volume;
 }

 public void setVolume(double volume) {
  this.volume = volume;
 }

 @Override
 public String toString() {
  return "Symbol: " + this.symbol + "\n" +
      "Time: " + this.time + "\n" +
      "Broker Commission: " + this.brokerCom + "\n" +
      "Trade Cost: " + this.tradeCost + "\n" +
     // "IOC Hit Rate: " + this.iocHitRate + "\n" +
      "High: " + this.high + "\n" +
      "Low: " + this.low + "\n" +
      "Open: " + this.open + "\n" +
      "Close: " + this.close + "\n" +
      "Volume: " + this.volume + "\n"; 
  
 }
}