package com.citi.entity;

import java.sql.Timestamp;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;


public class Trade {
	
	@Id	
	private int id;
	private Date time;
	private String sender;
	private String target;
	private String messageType;
	private String clientOrderID;
	private int timeInForce;
	private double avgPrice;
	private int cumQty;
	private int orderQty;
	private int orderStatus;
	
	private String broker;
	private String symbol;
	


	public Trade() {
		// Empty no-arg constructor.
	}
	
	public Trade(Date time, 
				 String clientOrderID, 
				 String broker, 
				 String symbol, 
				 int timeInForce, 
				 double avgPrice, 
				 int cumQty, 
				 int orderQty, 
				 int orderStatus){
		this.time = time;
		this.clientOrderID = clientOrderID;
		this.timeInForce = timeInForce;
		this.avgPrice = avgPrice;
		this.cumQty = cumQty;
		this.orderQty = orderQty;
		this.orderStatus = orderStatus;
		this.broker = broker;
		this.symbol = symbol;
	}
	
	public Trade(Date time, 
				 String sender, 
				 String target, 
				 String messageType, 
				 String clientOrderID,
				 String broker, 
				 String symbol, 
				 int timeInForce, 
				 double avgPrice, 
				 int cumQty, 
				 int orderQty, 
				 int orderStatus){
		this.time = time;
		this.sender = sender;
		this.target = target;
		this.messageType = messageType;
		this.clientOrderID = clientOrderID;
		this.timeInForce = timeInForce;
		this.avgPrice = avgPrice;
		this.cumQty = cumQty;
		this.orderQty = orderQty;
		this.orderStatus = orderStatus;
		this.broker = broker;
		this.symbol = symbol;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessage(String messageType) {
		this.messageType = messageType;
	}
	public String getClientOrderID() {
		return clientOrderID;
	}
	public void setClientOrderID(String clientOrderID) {
		this.clientOrderID = clientOrderID;
	}
	public int getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(int timeInForce) {
		this.timeInForce = timeInForce;
	}

	public double getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(double avgPrice) {
		this.avgPrice = avgPrice;
	}

	public int getCumQty() {
		return cumQty;
	}

	public void setCumQty(int cumQty) {
		this.cumQty = cumQty;
	}

	public int getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(int orderQty) {
		this.orderQty = orderQty;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}
	
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	@Override
	public String toString() {
		return "Time: " + this.time + "\n" +
			   "Sender: " + this.sender + "\n" +
			   "Target: " + this.target + "\n" +
			   "MessageType: " + this.messageType + "\n" +
			   "ClientOrderID: " + this.clientOrderID + "\n" +
			   "TimeInForce: " + this.timeInForce + "\n" +
			   "AvgPrice: " + this.avgPrice + "\n" +
			   "CumQty: " + this.cumQty + "\n" +
			   "OrderQty: " + this.orderQty + "\n" +
			   "OrderStatus: " + this.orderStatus + "\n" +
			   "Broker: " + this.broker + "\n" +
			   "Symbol: " + this.symbol + "\n";
	}
}
