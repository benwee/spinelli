package com.citi.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.citi.entity.Order;
import com.citi.entity.Trade;
import com.citi.repository.FixMongoRepository;
import com.citi.repository.FixRestRepository;
import com.citi.repository.FixRestTradeRepository;

@Service
public class FixRestService {
	@Autowired
	FixRestTradeRepository restRepo;

	@Autowired
	FixMongoRepository repo;
	
	@Autowired
	FixRestRepository orderRepo; 
	
	@Autowired
	FixRestTradeRepository tradeRepo; 


	@Value("${brokerCommissionPercentage}")
	private double commissionPerc;

	@Value("${tradeCostPercentage}")
	private double costPerc;

	public static ArrayList<Trade> tradeList = new ArrayList<Trade>();
	public static ArrayList<Order> orderList = new ArrayList<>();

	public List<Order> getAllOrders() {
		System.out.println("TESTTT");
		System.out.println(orderRepo.findAll().size());
		return orderRepo.findAll();
	}
	
	public List<Trade> getSomeTrades() {
		return tradeRepo.findTop100ByOrderStatus(2);
	}
	
	public List<Trade> getAllTrades() {
		return restRepo.findAll();
	}

	public Timestamp parseTime(String time) {
		Timestamp result = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date parseDate = dateFormat.parse(time);
			result = new java.sql.Timestamp(parseDate.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Map<String, Double> calculateCommission(String startTime, String endTime) {

		List<Trade> filledTrades = restRepo.findTradeByTimeBetweenAndOrderStatus(parseTime(startTime),
				parseTime(endTime), 2);

		Map<String, Double> commission = new HashMap<>();
		for (Trade t : filledTrades) {
			String symbol = t.getSymbol();
			double tradeValue = t.getAvgPrice() * t.getCumQty() * commissionPerc;
			if (!commission.containsKey(symbol)) {
				commission.put(symbol, tradeValue);
			} else {
				commission.put(symbol, commission.get(symbol) + tradeValue);
			}
		}

		return commission;
	}

	public Map<String, Double> calculateTradeCost(String startTime, String endTime) {
		List<Trade> filledTrades = restRepo.findTradeByTimeBetweenAndOrderStatus(parseTime(startTime),
				parseTime(endTime), 2);
		Map<String, Double> cost = new HashMap<>();
		for (Trade t : filledTrades) {
			String symbol = t.getSymbol();
			double tradeValue = t.getAvgPrice() * t.getCumQty() * costPerc;
			if (!cost.containsKey(symbol)) {
				cost.put(symbol, tradeValue);
			} else {
				cost.replace(symbol, cost.get(symbol) + tradeValue);
			}
		}
		return cost;
	}

	public Map<Date, Double> calculateIOChitRate() {
		Trade firstIOC = repo.findMinTimeOfNewIOC();
		Trade lastIOC = repo.findMaxTimeOfIOC();
		Date d1 = firstIOC.getTime();
		Date last = lastIOC.getTime();

		long timeInterval = 10 * 60 * 1000;
		Map<Date, Double> iocHitRate = new TreeMap<>();
		do {
			Date n = new Date(d1.getTime());
			d1.setTime(d1.getTime() + timeInterval);
			double rate = (double) repo.sumFilledIOCQtyByTimeBtw(n, d1) / repo.sumNewIOCQtyByTimeBtw(n, d1);
			iocHitRate.put(n, rate);
		} while (d1.before(last));

		return iocHitRate;

	}
}
