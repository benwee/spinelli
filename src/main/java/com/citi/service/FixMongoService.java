package com.citi.service;

import java.util.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.citi.entity.Order;
import com.citi.entity.Trade;
import com.citi.repository.FixMongoRepository;
import com.citi.repository.FixRestRepository;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.DB;
import com.mongodb.util.JSON;
import com.mongodb.*;

import quickfix.ConfigError;
import quickfix.DataDictionary;
import quickfix.Field;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.FieldType;
import quickfix.Group;
import quickfix.InvalidMessage;
import quickfix.MessageFactory;
import quickfix.MessageUtils;
import quickfix.field.BeginString;

@Service
public class FixMongoService {
	
	@Autowired
	FixMongoRepository repo;
		
	public static int counter = 1;
	public static SimpleDateFormat DATE_FORMAT = 
			new SimpleDateFormat("yyyyddMM-hh:mm:ss.SSS");
	public static final String TIME = "SendingTime";
	public static final String SENDER = "SenderCompID";
	public static final String TARGET= "TargetCompID";
	public static final String MESSAGE = "MsgType";
	public static final String CLIENT_ORDER_ID = "ClOrdID";
	public static final String EXECUTION_REPORT = "8";
	public static final String TIME_IN_FORCE = "TimeInForce";
	public static final String AVERAGE_PRICE = "AvgPx";
	public static final String CUM_QTY = "CumQty";
	public static final String ORDER_QTY = "OrderQty";
	public static final String ORDER_STATUS = "OrdStatus";
	public static final String BROKER = "ExecBroker";
	public static final String SYMBOL = "Symbol";
	
	@Value("${brokerCommissionPercentage}")
	private double commissionPerc;

	@Value("${tradeCostPercentage}")
	private double costPerc;
	
	public static ArrayList<Trade> tradeList = new ArrayList<Trade>();
	public static ArrayList<Order> orderList = new ArrayList<>();
	

	public FixMongoService() {
		
	}

	public void runOrder() {
		try {
			File f = new File("");
			String path = f.getAbsolutePath();
			MessageFactory messageFactory = new quickfix.fix42.MessageFactory();
			DataDictionary dataDictionary = new DataDictionary(path + "\\src\\main\\resources\\data\\D_FIX42Dictionary.xml");
			BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\DELL 2010\\Desktop\\Sample data\\sample.log"));
			
			Pattern fixMatchRegex = Pattern.compile(".*(8=FIX.*10=....).*");
			long startTime = System.currentTimeMillis();
	
			String sCurrentLine = null;
			int counter = 0;
			
			MongoClient mongo = new MongoClient("mysql.conygre.com", 27017);
			DB db = mongo.getDB("spinelli");
			
			DBCollection coll = db.getCollection("order");
			coll.drop();
	
			while ((sCurrentLine = reader.readLine()) != null) {
				Order currentTrade;
				Matcher matcher = fixMatchRegex.matcher(sCurrentLine);
				if (matcher.matches()) {
					
					String fixMessageString = matcher.group(1);
					quickfix.Message message = MessageUtils.parse(messageFactory, dataDictionary, fixMessageString);
					
					currentTrade = convertToJSONOrder(dataDictionary, message);
					
					if (currentTrade != null) {
						currentTrade.setId(counter);
						orderList.add(currentTrade);
						if (counter % 1000 == 0) {
							counter++;
							repo.insertManyOrderDocuments(orderList);
							orderList = new ArrayList<Order>(); //empty tradeList
						} else {
							counter++;
						}
					}					
				} 
			}
			if (tradeList.size() != 0)
				repo.insertManyDocuments(tradeList);
			//System.out.printf("There are now %d Trades\n", repository.count());
			System.out.println("TotalTime:" + (System.currentTimeMillis() - startTime));
			System.out.println(coll.count());
			reader.close();

		} catch (ConfigError e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (InvalidMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FieldNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void run() {
		try {
//			URL urlResult = getClass().getResource("C:\\citiprj\\data\\fixData\\result.txt");
//			URL urlSample = getClass().getResource("sample1.log");
//			URL urlDict = getClass().getResource("D_FIX42Dictionary.xml");			
			
			MessageFactory messageFactory = new quickfix.fix42.MessageFactory();
			//DataDictionary dataDictionary = new DataDictionary("C:\\Users\\Administrator\\Desktop\\Sample data\\D_FIX42Dictionary.xml");
			//BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\Sample data\\sample.log"));
			DataDictionary dataDictionary = new DataDictionary("C:\\Users\\DELL 2010\\Desktop\\Sample data\\D_FIX42Dictionary.xml");
			BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\DELL 2010\\Desktop\\Sample data\\sample.log"));
			
			
			//BufferedReader reader2 = new BufferedReader(new FileReader("C:\\citiprj\\data\\fixData\\one_thousand.txt"));
			
			
			Pattern fixMatchRegex = Pattern.compile(".*(8=FIX.*10=....).*");
			long startTime = System.currentTimeMillis();
			
			System.out.println("START");
			//Future<String> wait = asyncToPopulateCollections(reader, fixMatchRegex, messageFactory,dataDictionary);
			//wait.get();
			String sCurrentLine = null;
			int counter = 0;
			
			//MongoClient mongo = new MongoClient("mysql.conygre.com", 27017);
			MongoClient mongo = new MongoClient("localhost", 27017);
			DB db = mongo.getDB("spinelli");
			
			DBCollection coll = db.getCollection("trade");
			coll.drop();

			while ((sCurrentLine = reader.readLine()) != null) {

				Trade currentTrade;
				Matcher matcher = fixMatchRegex.matcher(sCurrentLine);
				if (matcher.matches()) {
					
					String fixMessageString = matcher.group(1);
					quickfix.Message message = MessageUtils.parse(messageFactory, dataDictionary, fixMessageString);
					currentTrade = convertToJSON(dataDictionary, message);
					
					if (currentTrade != null) {
						currentTrade.setId(counter);
						tradeList.add(currentTrade);
						if (counter % 1000 == 0) {
							counter++;
							repo.insertManyDocuments(tradeList);
							tradeList = new ArrayList<Trade>(); //empty tradeList
						} else {
							counter++;
						}
					}					
				} 
			}
			if (tradeList.size() != 0)
				repo.insertManyDocuments(tradeList);
			//System.out.printf("There are now %d Trades\n", repository.count());
			System.out.println("TotalTime:" + (System.currentTimeMillis() - startTime));
			System.out.println(coll.count());
			reader.close();

		} catch (ConfigError e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (InvalidMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FieldNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
//	@Async
//	public Future<String> asyncToPopulateCollections(BufferedReader reader, Pattern fixMatchRegex, MessageFactory messageFactory, DataDictionary dataDictionary) throws IOException, InvalidMessage, FieldNotFound {
//		String sCurrentLine = null;
//		while ((sCurrentLine = reader.readLine()) != null) {
//			//System.out.println("Current Line:" + sCurrentLine);
//			Matcher matcher = fixMatchRegex.matcher(sCurrentLine);
//			if (matcher.matches()) {
//				String fixMessageString = matcher.group(1);
//				//System.out.println("FIX String:" + fixMessageString);
//
//				quickfix.Message message = MessageUtils.parse(messageFactory, dataDictionary, fixMessageString);
//				System.out.println("here");
//				
//				convertToJSON(dataDictionary, message);
//				
//			}
//			
//		}
//		
//		return new AsyncResult("Finished");
//	}
	private static String buildJSONString(String orig) {
		return "\"" + orig + "\"";
	}
	
	private Order convertToJSONOrder(DataDictionary dataDictionary, quickfix.Message message)
			throws FieldNotFound, IOException {

		// long startTime = System.nanoTime();
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
		ObjectNode rootNode = mapper.createObjectNode();
		ObjectNode headerNode = rootNode.putObject("header");
		convertFieldMapToJSON(dataDictionary, message.getHeader(), headerNode);
		convertFieldMapToJSON(dataDictionary, message, rootNode);
		
		JsonNode currentTransaction = rootNode.get("header");
		JsonNode outsideHeader = rootNode;
		Order currentTrade = null;
		//System.out.println(currentTransaction);
		
		if (outsideHeader.has(ORDER_STATUS) && outsideHeader.get(ORDER_STATUS).asInt() == 2 && currentTransaction.has(MESSAGE) && currentTransaction.get(MESSAGE).asText().equals(EXECUTION_REPORT)) {
			Date currentDate = new Date();
			//System.out.println("reach");
			try {
				currentDate = DATE_FORMAT.parse(currentTransaction.get("SendingTime").asText());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			long time = currentDate.getTime();
			currentTrade = new Order();
			/*currentTrade = new Order(new Timestamp(time), 
									 //currentTransaction.get(SENDER).asText(),
									 //currentTransaction.get(TARGET).asText(),
									 //currentTransaction.get(MESSAGE).asText(),
									 //outsideHeader.get(CLIENT_ORDER_ID).asText(),
									 //outsideHeader.get(BROKER).asText(),
									 outsideHeader.get(SYMBOL).asText(),
									 //outsideHeader.get(TIME_IN_FORCE).asInt(),
									 outsideHeader.get(AVERAGE_PRICE).asDouble(),
									 outsideHeader.get(CUM_QTY).asInt(),
									 outsideHeader.get(ORDER_QTY).asInt()
									 //outsideHeader.get(ORDER_STATUS).asInt()
									 );*/
			
			//tradeList.add(currentTrade);
			//repository.insert(currentTrade);
			//System.out.println(currentTrade);
		}
		
		return currentTrade;
		// System.out.println(System.nanoTime()- startTime);

	}

	private Trade convertToJSON(DataDictionary dataDictionary, quickfix.Message message)
			throws FieldNotFound, IOException {

		// long startTime = System.nanoTime();
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
		ObjectNode rootNode = mapper.createObjectNode();
		ObjectNode headerNode = rootNode.putObject("header");
		convertFieldMapToJSON(dataDictionary, message.getHeader(), headerNode);
		convertFieldMapToJSON(dataDictionary, message, rootNode);
		
		JsonNode currentTransaction = rootNode.get("header");
		JsonNode outsideHeader = rootNode;
		Trade currentTrade = null;
		//System.out.println(currentTransaction);
		
		if (currentTransaction.has(MESSAGE) && currentTransaction.get(MESSAGE).asText().equals(EXECUTION_REPORT)) {
			Date currentDate = new Date();
			//System.out.println("reach");
			try {
				currentDate = DATE_FORMAT.parse(currentTransaction.get("SendingTime").asText());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			long time = currentDate.getTime();
			currentTrade = new Trade(new Timestamp(time), 
									 currentTransaction.get(SENDER).asText(),
									 currentTransaction.get(TARGET).asText(),
									 currentTransaction.get(MESSAGE).asText(),
									 outsideHeader.get(CLIENT_ORDER_ID).asText(),
									 outsideHeader.get(BROKER).asText(),
									 outsideHeader.get(SYMBOL).asText(),
									 outsideHeader.get(TIME_IN_FORCE).asInt(),
									 outsideHeader.get(AVERAGE_PRICE).asDouble(),
									 outsideHeader.get(CUM_QTY).asInt(),
									 outsideHeader.get(ORDER_QTY).asInt(),
									 outsideHeader.get(ORDER_STATUS).asInt()
									 );
			
			//tradeList.add(currentTrade);
			//repository.insert(currentTrade);
			//System.out.println(currentTrade);
		}
		
		return currentTrade;
		// System.out.println(System.nanoTime()- startTime);

	}

	private static void convertFieldMapToJSON(DataDictionary dataDictionary, FieldMap fieldmap, ObjectNode node)
			throws FieldNotFound {
		Iterator<Field<?>> fieldIterator = fieldmap.iterator();
		while (fieldIterator.hasNext()) {
			Field field = (Field) fieldIterator.next();
			String value = fieldmap.getString(field.getTag());
			if (!isGroupCountField(dataDictionary, field)) {
				node.put(String.valueOf(field.getTag()), value);
				
			String fieldName = dataDictionary.getFieldName(field.getTag()); if (fieldName
				 == null){ fieldName = "UDF"+ field.getTag();} node.put(fieldName, value);	 
			}
		}

		Iterator groupsKeys = fieldmap.groupKeyIterator();
		while (groupsKeys.hasNext()) {
			int groupCountTag = ((Integer) groupsKeys.next()).intValue();
			// System.out.println(groupCountTag + ": count = "
			// + fieldMap.getInt(groupCountTag));
			Group group = new Group(groupCountTag, 0);
			ArrayNode repeatingGroup = node.putArray(String.valueOf(groupCountTag));
			/*
			 * String fieldName = dataDictionary.getFieldName(groupCountTag); if (fieldName
			 * == null){ fieldName = "UDF"+ groupCountTag;} ArrayNode repeatingGroup =
			 * node.putArray(fieldName);
			 */
			int i = 1;
			while (fieldmap.hasGroup(i, groupCountTag)) {
				fieldmap.getGroup(i, group);
				ObjectNode groupNode = repeatingGroup.addObject();
				convertFieldMapToJSON(dataDictionary, group, groupNode);
				i++;
			}
		}
	}

	private static boolean isGroupCountField(DataDictionary dd, Field field) {
		return dd.getFieldTypeEnum(field.getTag()) == FieldType.NumInGroup;

	}

}
