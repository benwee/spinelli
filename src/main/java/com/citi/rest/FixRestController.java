package com.citi.rest;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.entity.Order;
import com.citi.entity.Trade;
import com.citi.service.FixMongoService;
import com.citi.service.FixRestService;

@RestController
@CrossOrigin
public class FixRestController {
//	@Autowired
//	private FixService service;
	
	@Autowired
	private FixRestService mongoService;
	
	@RequestMapping(method=RequestMethod.GET, value="/orders", 
            headers="Accept=application/json, text/plain")
	public List<Order> getAllOrders() {
		//System.out.println(mongoService.getAllOrders());
		System.out.println("ADASDSADASDAS"); 
		return mongoService.getAllOrders();
	}

	@RequestMapping(method=RequestMethod.GET, value="/someTrade", 
            headers="Accept=application/json, text/plain")
	public List<Trade> getSomeTrades() {
		return mongoService.getSomeTrades();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/trades", 
            headers="Accept=application/json, text/plain")
	public List<Trade> getAllTrades() {
		return mongoService.getAllTrades();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/tradesCommission/{startTime}/{endTime}", 
            headers="Accept=application/json, text/plain")
	public Map<String,Double> getCommission(@PathVariable String startTime, @PathVariable String endTime ) {
		return mongoService.calculateCommission(startTime, endTime);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/tradesCost/{startTime}/{endTime}", 
            headers="Accept=application/json, text/plain")
	public Map<String,Double> getTradeCost(@PathVariable String startTime, @PathVariable String endTime ) {
		return mongoService.calculateTradeCost(startTime, endTime);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/iocHitRate", 
            headers="Accept=application/json, text/plain")
	public Map<Date,Double> getTradeCost() {
		return mongoService.calculateIOChitRate();
	}
}
