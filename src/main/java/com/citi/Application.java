package com.citi;

import java.io.File;
import java.nio.file.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		
		//Watcher Service
		/*WatchService watchService = FileSystems.getDefault().newWatchService();
	
			File f = new File("");
			String abs_path = f.getAbsolutePath();
	      Path path = Paths.get(abs_path + "\\src\\resources\\main\\data");
	
	      path.register(
	        watchService, 
	          StandardWatchEventKinds.ENTRY_CREATE, 
	            StandardWatchEventKinds.ENTRY_DELETE, 
	              StandardWatchEventKinds.ENTRY_MODIFY);
	
	      WatchKey key;
	      while ((key = watchService.take()) != null) {
	          for (WatchEvent<?> event : key.pollEvents()) {
	              System.out.println(
	                "Event kind:" + event.kind() 
	                  + ". File affected: " + event.context() + ".");
	          }
	          key.reset();
	      }*/
  
	}
}
